package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.enemy.Not;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.missile.Missile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.missile.SmallMissile;
import org.junit.Assert;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

public class GameObjectTest extends ApplicationTest {

    @Test
    public void collidesWithTest_1() {
        Not enemy = new Not(new Position(0,0));
        Missile missile = new SmallMissile(new Position(0,0), null);

        Assert.assertTrue(enemy.collidesWith(missile));
    }

    @Test
    public void collidesWithTest_2() {
        Not enemy = new Not(new Position(0,0));
        Missile missile = new SmallMissile(new Position(enemy.getWidth(),0), null);

        Assert.assertTrue(enemy.collidesWith(missile));
    }

    @Test
    public void collidesWithTest_Not() {
        Not enemy = new Not(new Position(0,0));
        Missile missile = new SmallMissile(new Position(enemy.getWidth() + 1,0), null);

        Assert.assertFalse(enemy.collidesWith(missile));
    }

    @Test
    public void collidesWithTest_Move() {
        Not enemy = new Not(new Position(0,0));
        Missile missile = new SmallMissile(new Position(enemy.getWidth(),enemy.getHeight() + 1), null);

        Assert.assertFalse(enemy.collidesWith(missile));

        missile.move(0, -1);
        Assert.assertTrue(enemy.collidesWith(missile));
    }

    @Test
    public void collidesWithTest_MoveToSide() {
        Not enemy = new Not(new Position(0,0));
        Missile missile = new SmallMissile(new Position(enemy.getWidth(),0), null);

        Assert.assertTrue(enemy.collidesWith(missile));

        missile.move(missile.getWidth(), 0);
        Assert.assertFalse(enemy.collidesWith(missile));
    }
}
