package cz.cvut.fit.miadp;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.view.GameView;
import org.junit.Test;

public class ModelTest {

    @Test
    public void registerObserverTest() {
        GameModel model = mock(GameModel.class);
        GameView view = new GameView(model);

        verify(model).registerObserver(view);
    }
}
