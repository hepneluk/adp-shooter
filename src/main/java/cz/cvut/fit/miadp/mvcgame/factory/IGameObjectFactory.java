package cz.cvut.fit.miadp.mvcgame.factory;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.collision.Collision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.Player;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.canon.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.enemy.Enemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.gameInfo.GameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.gameInfo.ScoreCounter;

public interface IGameObjectFactory {
    Enemy createEnemy();
    Collision createCollision();
    GameInfo createGameInfo();
    Player createPlayer();
    AbsCannon createCannon(Position position);

    ScoreCounter createScoreCounter();
}
