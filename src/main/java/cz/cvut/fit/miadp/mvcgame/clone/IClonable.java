package cz.cvut.fit.miadp.mvcgame.clone;

public interface IClonable {
    Object getClone();
}
