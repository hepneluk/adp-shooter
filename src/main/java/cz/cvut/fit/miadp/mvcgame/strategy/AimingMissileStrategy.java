package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;

public class AimingMissileStrategy extends AimMoveStrategy {
    private static AimingMissileStrategy instance = null;

    private AimingMissileStrategy() {
    }

    @Override
    public void updatePosition(Position position, GameObject gameObject) {
        if (gameObject != null)
            headTowards(position, gameObject, MvcGameConfig.MISSILE_SPEED);
        else
            position.move(0, -MvcGameConfig.MISSILE_SPEED);
    }

    public static AimingMissileStrategy getInstance() {
        if (instance == null)
            instance = new AimingMissileStrategy();
        return instance;
    }
}
