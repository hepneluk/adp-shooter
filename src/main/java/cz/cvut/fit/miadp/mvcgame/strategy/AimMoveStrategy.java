package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;

public abstract class AimMoveStrategy implements IMoveStrategy {
    public void headTowards(Position position, GameObject gameObject, int speed) {
        int distanceX = gameObject.getPosition().getX() - position.getX() + gameObject.getWidth();
        int distanceY = gameObject.getPosition().getY() - position.getY() + gameObject.getHeight();

        double distance = Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2));
        double divider = distance / speed;
        double moveX = ((double)distanceX) / divider;
        double moveY = ((double)distanceY) / divider;

        // make sure the speed is not lower
        if (moveX < moveY) {
            distanceX = (int) Math.floor(moveX);
            distanceY = (int) Math.ceil(moveY);
        }
        else {
            distanceX = (int) Math.ceil(moveX);
            distanceY = (int) Math.floor(moveY);
        }

        position.move(distanceX, distanceY);
    }

    @Override
    public int value() {
        return 2;
    }
}
