package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public class PlayerMoveDown extends AbsCommand {
    public PlayerMoveDown(IGameModel rec) {
        super(rec);
    }

    @Override
    public void execute() {
        receiver.movePlayerDown();
    }
}
