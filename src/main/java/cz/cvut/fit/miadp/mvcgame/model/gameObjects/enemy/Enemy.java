package cz.cvut.fit.miadp.mvcgame.model.gameObjects.enemy;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.missile.Missile;
import cz.cvut.fit.miadp.mvcgame.strategy.IMoveStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SlowEnemyMoveStrategy;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public abstract class Enemy extends GameObject{
    protected String imagePath;
    protected int health;
    protected int maxHealth;
    protected boolean dead = false;
    protected IMoveStrategy moveStrategy;

    public Enemy(Position position, int width, int height, int health, int maxHealth) {
        super(position, width, height);
        this.health = health;
        this.maxHealth = maxHealth;
        this.moveStrategy = SlowEnemyMoveStrategy.getInstance();
    }

    public Enemy(Position position, int width, int height, int health, int maxHealth, IMoveStrategy moveStrategy) {
        super(position, width, height);
        this.health = health;
        this.maxHealth = maxHealth;
        this.moveStrategy = moveStrategy;
    }

    public int getHealth() {
        return health;
    }

    public void gotHit(Missile missile) {
        health -= missile.getDamage();
        if (health <= 0) {
            dead = true;
        }
    }

    public boolean isDead() {
        return dead;
    }

    public boolean isObsolete() {
        return dead || isOutOfView();
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visitEnemy(this);
    }

    @Override
    public boolean isOutOfView() {
        // enemies are spawned out of view
        return super.isOutOfView() && !(position.getY() < 0);
    }

    public int getValue() {
        return maxHealth * moveStrategy.value();
    }

    public void move(GameObject gameObject) {
        moveStrategy.updatePosition(position, gameObject);
    }

    @Override
    public abstract Object clone();

    public String getImagePath() {
        return imagePath;
    }

    @Override
    public Object getClone() {
        return clone();
    }
}
