package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;

public class SlowEnemyMoveStrategy implements IMoveStrategy {
    private static SlowEnemyMoveStrategy instance = null;

    private SlowEnemyMoveStrategy() {
    }

    @Override
    public void updatePosition(Position position, GameObject gameObject) {
        position.move(0, MvcGameConfig.ENEMY_SPEED_SLOW);
    }

    @Override
    public int value() {
        return 1;
    }

    public static SlowEnemyMoveStrategy getInstance() {
        if (instance == null)
            instance = new SlowEnemyMoveStrategy();
        return instance;
    }
}
