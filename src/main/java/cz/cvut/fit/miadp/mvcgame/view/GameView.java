package cz.cvut.fit.miadp.mvcgame.view;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.controller.GameController;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;
import cz.cvut.fit.miadp.mvcgame.visitor.RenderingVisitor;
import javafx.scene.canvas.GraphicsContext;

public class GameView  implements IObserver {
    private IGameModel model;
    private IGameGraphics gr;
    private int updateCnt = 1;

    private IVisitor renderingVisitor;

    public GameView(IGameModel model) {
        this.model = model;
        model.registerObserver(this);

        renderingVisitor = new RenderingVisitor();
    }

    public GameController makeController() {
        return new GameController(model);
    }

    public void update() {
        updateCnt++;
    }

    public void render() {
        if (updateCnt > 0) {
            if (gr == null)
                return;

            // clear canvas
            gr.clearRect(new Position(0, 0), new Position(MvcGameConfig.MAX_X, MvcGameConfig.MAX_Y));

            for (GameObject go : model.getGameObjects()) {
                go.accept(renderingVisitor);
            }

            updateCnt = 0;
        }
    }

    public void setGraphics(IGameGraphics gr) {
        renderingVisitor.setGraphics(gr);
        this.gr = gr;
    }
}
