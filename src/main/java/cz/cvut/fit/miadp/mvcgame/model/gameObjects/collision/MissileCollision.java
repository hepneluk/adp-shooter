package cz.cvut.fit.miadp.mvcgame.model.gameObjects.collision;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public class MissileCollision extends Collision{
    public MissileCollision(Position position, int width, int height) {
        super(position, width, height);
    }

    public MissileCollision(Position position) {
        super(position, 10, 10);
    }

    @Override
    public void accept(IVisitor visitor) {
        width += 2;
        height += 2;
        super.accept(visitor);
    }

    @Override
    public String toString() {
        return super.toString() + "; width: " + width + ", height: " + height + ", age: " + getAge() + ", vs " + MvcGameConfig.COLLISION_DURATION;
    }

    @Override
    protected Object clone() {
        MissileCollision mc = new MissileCollision(getPosition(), width, height);
        mc.setBornAt(bornAt);
        return mc;
    }
}
