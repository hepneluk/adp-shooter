package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.command.AbsCommand;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;

import java.util.List;

public class GameModelProxy implements IGameModel {
    private GameModel model = new GameModel();

    @Override
    public void registerCmd(AbsCommand command) {
        model.registerCmd(command);
    }

    @Override
    public void goBackInTime() {
        model.goBackInTime();
    }

    @Override
    public void timeTick() {
        model.timeTick();
    }

    @Override
    public void movePlayerUp() {
        model.movePlayerUp();
    }

    @Override
    public void movePlayerDown() {
        model.movePlayerDown();
    }

    @Override
    public void movePlayerLeft() {
        model.movePlayerLeft();
    }

    @Override
    public void movePlayerRight() {
        model.movePlayerRight();
    }

    @Override
    public void registerObserver(IObserver observer) {
        model.registerObserver(observer);
    }

    @Override
    public void unregisterObserver(IObserver observer) {
        model.unregisterObserver(observer);
    }

    @Override
    public void notifyMyObservers() {
        model.notifyMyObservers();
    }

    @Override
    public List<GameObject> getGameObjects() {
        return model.getGameObjects();
    }

    @Override
    public void togglePlayerShootingMode() {
        model.togglePlayerShootingMode();
    }

    @Override
    public void update() {
        model.update();
    }

    @Override
    public Object createMemento() {
        return model.createMemento();
    }

    @Override
    public void setMemento(Object object) {
        model.setMemento(object);
    }
}
