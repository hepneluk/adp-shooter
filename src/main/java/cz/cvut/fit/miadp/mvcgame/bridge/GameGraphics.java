package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.model.Position;

public class GameGraphics implements IGameGraphics {
    protected IGameGraphicsImplementor implementor;

    public GameGraphics(IGameGraphicsImplementor implementor) {
        this.implementor = implementor;
    }

    @Override
    public void drawImage(String path, Position pos) {
        implementor.drawImage(path, pos);
    }

    @Override
    public void drawText(String text, Position pos) {
        implementor.drawText(text, pos);
    }

    @Override
    public void drawRectangle(Position leftTop, Position rightBottom) {
        Position rightTop = new Position(rightBottom.getX(), leftTop.getY());
        Position leftBottom = new Position(leftTop.getX(), rightBottom.getY());
        implementor.drawLine(leftTop, rightTop);
        implementor.drawLine(leftTop, leftBottom);
        implementor.drawLine(rightBottom, rightTop);
        implementor.drawLine(rightBottom, leftBottom);
    }

    @Override
    public void clearRect(Position leftTop, Position rightBottom) {
        implementor.clearRect(leftTop, rightBottom);
    }

    @Override
    public void strokeText(String text, Position pos) {
        implementor.strokeText(text, pos);
    }

    @Override
    public void fillRect(Position leftTop, Position bottomRight) {
        implementor.fillRect(leftTop, bottomRight);
    }

    @Override
    public void fillRect(Position leftTop, int width, int height) {
        implementor.fillRect(leftTop, width, height);
    }

    @Override
    public void setStroke(String color) {
        implementor.setStroke(color);
    }

    @Override
    public void strokeLine(Position start, Position end) {
        implementor.strokeLine(start, end);
    }
}
