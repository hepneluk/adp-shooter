package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;

public class AimingEnemyStrategyNormal extends  AimMoveStrategy {
    private static AimingEnemyStrategyNormal instance = null;

    private AimingEnemyStrategyNormal() {
    }

    @Override
    public void updatePosition(Position position, GameObject gameObject) {
        super.headTowards(position, gameObject, MvcGameConfig.ENEMY_SPEED_NORMAL);
    }

    @Override
    public int value() {
        return 4;
    }

    public static AimingEnemyStrategyNormal getInstance() {
        if (instance == null)
            instance = new AimingEnemyStrategyNormal();
        return instance;
    }
}
