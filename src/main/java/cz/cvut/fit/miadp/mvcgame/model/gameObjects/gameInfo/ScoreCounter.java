package cz.cvut.fit.miadp.mvcgame.model.gameObjects.gameInfo;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.observer.IObservable;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

import java.util.ArrayList;
import java.util.List;

public class ScoreCounter extends GameInfo implements IObservable {
    private int score = 0;
    private List<IObserver> myObservers = new ArrayList<>();

    public ScoreCounter() {
        super(new Position(MvcGameConfig.SCORE_COUNTER_X, MvcGameConfig.SCORE_COUNTER_Y));
    }

    public void addScore(int x) {
        score += x;
        if (score % 50 == 0) {
            notifyMyObservers();
        }
    }

    public int getScore() {
        return score;
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visitScoreInfo(this);
    }

    @Override
    public void registerObserver(IObserver observer) {
        myObservers.add(observer);
    }

    @Override
    public void unregisterObserver(IObserver observer) {
        myObservers.remove(observer);
    }

    @Override
    public void notifyMyObservers() {
        for (IObserver obs : myObservers) {
            obs.update();
        }
    }

    @Override
    protected Object clone() {
        ScoreCounter scoreCounter = new ScoreCounter();
        scoreCounter.addScore(score);
        for (IObserver o : myObservers)
            scoreCounter.registerObserver(o);
        return scoreCounter;
    }
}
