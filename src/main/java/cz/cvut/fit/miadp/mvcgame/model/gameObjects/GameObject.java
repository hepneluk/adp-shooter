package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.clone.IClonable;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public abstract class GameObject implements Cloneable, IClonable {
    protected Position position;
    protected int width = 0;
    protected int height = 0;

    public GameObject(Position position) {
        this.position = position;
    }

    public GameObject(Position position, int width, int height) {
        this.position = position;
        this.width = width;
        this.height = height;
    }

    public abstract void accept(IVisitor visitor);

    public boolean collidesWith(GameObject other) {
        return containsObject(other) || other.containsObject(this);
    }

    public void move(int dx, int dy) {
        position.setX(position.getX() + dx);
        position.setY(position.getY() + dy);
    }

    public void move(Position position) {
        move(position.getX(), position.getY());
    }

    public Position getPosition() {
        return new Position(position);
    }

    public boolean isOutOfView() {
        return position.getX() > MvcGameConfig.MAX_X || position.getX() < 0 ||
                position.getY() > MvcGameConfig.MAX_Y || position.getY() < 0;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Position getTopLeft() {
        return new Position(position.getX(), position.getY());
    }

    public Position getTopRight() {
        return new Position(position.getX() + width, position.getY());
    }

    public Position getBottomLeft() {
        return new Position(position.getX(), position.getY() + height);
    }

    public Position getBottomRight() {
        return new Position(position.getX() + width, position.getY() + height);
    }

    protected boolean isPointInside(Position pos) {
        return getTopLeft().getY() <= pos.getY() &&
               getBottomLeft().getY() >= pos.getY() &&
               getTopLeft().getX() <= pos.getX() &&
               getTopRight().getX() >= pos.getX();
    }

    protected boolean containsObject(GameObject other) {
        return isPointInside(other.getTopLeft()) ||
               isPointInside(other.getTopRight()) ||
               isPointInside(other.getBottomLeft()) ||
               isPointInside(other.getBottomRight());
    }
}
