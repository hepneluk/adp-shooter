package cz.cvut.fit.miadp.mvcgame.builder;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.enemy.And;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.enemy.Enemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.enemy.Not;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.enemy.Or;
import cz.cvut.fit.miadp.mvcgame.strategy.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class EnemyPlacer {
    private int level;
    private int maxValue;
    private int value;
    private List<Enemy> availableEnemies;
    private Random random = new Random();

    public EnemyPlacer() {
        reset();

        Position pos = new Position(0,0);
        this.availableEnemies = new ArrayList<>();
        availableEnemies.add(new Not(pos));
        availableEnemies.add(new And(pos));
        availableEnemies.add(new Or(pos));
        availableEnemies.add(new Not(pos, AimingEnemyStrategy.getInstance()));
        availableEnemies.add(new And(pos, AimingEnemyStrategy.getInstance()));
        availableEnemies.add(new Or(pos, AimingEnemyStrategy.getInstance()));
        availableEnemies.add(new Not(pos, AimingEnemyStrategyNormal.getInstance()));
        availableEnemies.add(new And(pos, AimingEnemyStrategyNormal.getInstance()));
        availableEnemies.add(new Or(pos, AimingEnemyStrategyNormal.getInstance()));
        availableEnemies.add(new Not(pos, AimingEnemyStrategyFast.getInstance()));
        availableEnemies.add(new And(pos, AimingEnemyStrategyFast.getInstance()));
        availableEnemies.add(new Or(pos, AimingEnemyStrategyFast.getInstance()));
        availableEnemies.add(new Not(pos, FastEnemyMoveStrategy.getInstance()));
        availableEnemies.add(new And(pos, FastEnemyMoveStrategy.getInstance()));
        availableEnemies.add(new Or(pos, FastEnemyMoveStrategy.getInstance()));
        availableEnemies.add(new Not(pos, NormalSpeedEnemyStrategy.getInstance()));
        availableEnemies.add(new And(pos, NormalSpeedEnemyStrategy.getInstance()));
        availableEnemies.add(new Or(pos, NormalSpeedEnemyStrategy.getInstance()));

        availableEnemies.sort(
                (e1, e2) -> e2.getValue() - e1.getValue());
    }

    public void reset() {
        this.level = MvcGameConfig.LEVEL_START;
        this.maxValue = MvcGameConfig.VALUE_OF_ENEMIES_START;
        this.value = 0;
    }

    public List<Enemy> createEnemies() {
        List<Enemy> enemies = new ArrayList<>();
        while (maxValue - value >= level || maxValue - value >= availableEnemies.get(0).getValue()) {
            enemies.add(newEnemy(maxValue - value));
        }

        return enemies;
    }

    public void enemyOut(Enemy enemy, List<Enemy> enemies) {
        value -= enemy.getValue();
        while (maxValue - value >= level || maxValue - value >= availableEnemies.get(0).getValue()) {
            enemies.add(newEnemy(maxValue - value));
        }
    }

    private Enemy newEnemy(int val) {
        List<Enemy> candidates = new ArrayList<>();
        for (Enemy e : availableEnemies) {
            // add most valuable enemy
            if (e.getValue() <= val && e.getValue() <= level) {
                candidates.add(e);
            }
        }

        assert !candidates.isEmpty() : "No available enemy";

        int index = random.nextInt(candidates.size());
        value += candidates.get(index).getValue();
        return setEnemyPosition((Enemy) candidates.get(index).clone());
    }

    private Enemy setEnemyPosition(Enemy enemy) {
        // TODO: more complicated distribution strategy
        // TODO: prevent collisions
        enemy.move(random.nextInt(MvcGameConfig.MAX_X - enemy.getWidth()),
                   -enemy.getHeight());
        return enemy;
    }

    public void nextLevel() {
        level++;
        maxValue = level * MvcGameConfig.VALUE_OF_ENEMIES_START;
    }
}
