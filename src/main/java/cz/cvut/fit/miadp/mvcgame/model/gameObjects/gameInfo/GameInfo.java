package cz.cvut.fit.miadp.mvcgame.model.gameObjects.gameInfo;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public abstract class GameInfo extends GameObject {

    public GameInfo(Position position) {
        super(position);
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visitGameInfo(this);
    }

    @Override
    protected abstract Object clone();

    @Override
    public Object getClone() {
        return clone();
    }
}
