package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;

public class AimingEnemyStrategy extends AimMoveStrategy {
    private static AimingEnemyStrategy instance = null;

    private AimingEnemyStrategy() {
    }

    @Override
    public void updatePosition(Position position, GameObject gameObject) {
        if (gameObject != null)
            headTowards(position, gameObject, MvcGameConfig.ENEMY_SPEED_SLOW);
        else
            position.move(0, MvcGameConfig.ENEMY_SPEED_SLOW);
    }

    public static AimingEnemyStrategy getInstance() {
        if (instance == null)
            instance = new AimingEnemyStrategy();
        return instance;
    }
}
