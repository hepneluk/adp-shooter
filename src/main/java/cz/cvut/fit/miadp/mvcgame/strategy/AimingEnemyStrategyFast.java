package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;

public class AimingEnemyStrategyFast extends AimMoveStrategy {
    private static AimingEnemyStrategyFast instance = null;

    private AimingEnemyStrategyFast() {
    }

    @Override
    public void updatePosition(Position position, GameObject gameObject) {
        super.headTowards(position, gameObject, MvcGameConfig.ENEMY_SPEED_FAST);
    }

    @Override
    public int value() {
        return 5;
    }

    public static AimingEnemyStrategyFast getInstance() {
        if (instance == null)
            instance = new AimingEnemyStrategyFast();
        return instance;
    }
}
