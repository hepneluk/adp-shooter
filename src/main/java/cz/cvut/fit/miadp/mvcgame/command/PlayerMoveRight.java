package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public class PlayerMoveRight extends AbsCommand {
    public PlayerMoveRight(IGameModel rec) {
        super(rec);
    }

    @Override
    public void execute() {
        receiver.movePlayerRight();
    }

}
