package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.model.Position;

public interface IGameGraphics {
    void drawImage(String path, Position pos);
    void drawText(String text, Position pos);
    void strokeText(String text, Position pos);
    void drawRectangle(Position leftTop, Position bottomRight);
    void fillRect(Position leftTop, Position bottomRight);
    void fillRect(Position leftTop, int width, int height);
    void clearRect(Position leftTop, Position rightBottom);
    void setStroke(String color);
    void strokeLine(Position start, Position end);
}
