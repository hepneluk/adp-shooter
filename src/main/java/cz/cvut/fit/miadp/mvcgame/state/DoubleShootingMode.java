package cz.cvut.fit.miadp.mvcgame.state;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.canon.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.missile.Missile;

import java.util.List;

public class DoubleShootingMode implements IShootingMode {
    @Override
    public List<Missile> shoot(AbsCannon canon) {
        return canon.doubleShoot();
    }

    @Override
    public void toggle(AbsCannon canon) {
        canon.setAimedShootingMode();
    }
}
