package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;

public class SimpleMissileMoveStrategy implements IMoveStrategy {
    // TODO: update to take missile
    private static SimpleMissileMoveStrategy instance = null;

    private SimpleMissileMoveStrategy() {
    }

    @Override
    public void updatePosition(Position position, GameObject gameObject) {
        position.move(0, -MvcGameConfig.MISSILE_SPEED);
    }

    @Override
    public int value() {
        return 1;
    }

    public static SimpleMissileMoveStrategy getInstance() {
        if (instance == null)
            instance = new SimpleMissileMoveStrategy();
        return instance;
    }
}
