package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public class PlayerMoveUp extends AbsCommand {
    public PlayerMoveUp(IGameModel rec) {
        super(rec);
    }

    @Override
    public void execute() {
        receiver.movePlayerUp();
    }
}
