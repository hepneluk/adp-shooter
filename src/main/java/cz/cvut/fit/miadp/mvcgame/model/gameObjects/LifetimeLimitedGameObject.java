package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.Position;

import java.time.LocalDateTime;

public abstract class LifetimeLimitedGameObject extends GameObject {
    protected long bornAt;

    public LifetimeLimitedGameObject(Position position, int width, int height) {
        super(position, width, height);
        bornAt = System.currentTimeMillis();
    }

    public long getAge() {
        return System.currentTimeMillis() - bornAt;
    }

    public abstract boolean isObsolete();

    protected void setBornAt(long bornAt) {
        this.bornAt = bornAt;
    }
}
