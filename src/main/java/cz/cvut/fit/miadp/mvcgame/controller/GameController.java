package cz.cvut.fit.miadp.mvcgame.controller;

import cz.cvut.fit.miadp.mvcgame.command.PlayerMoveDown;
import cz.cvut.fit.miadp.mvcgame.command.PlayerMoveLeft;
import cz.cvut.fit.miadp.mvcgame.command.PlayerMoveRight;
import cz.cvut.fit.miadp.mvcgame.command.PlayerMoveUp;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public class GameController {
    private IGameModel model;

    public GameController(IGameModel model) {
        this.model = model;
    }

    public void handleKeyCode(String keyCode) {
        switch(keyCode){
            case "UP":
                model.registerCmd(new PlayerMoveUp(model));
//                model.movePlayerUp();
                break;
            case "DOWN":
                model.registerCmd(new PlayerMoveDown(model));
//                model.movePlayerDown();
                break;
            case "LEFT":
                model.registerCmd(new PlayerMoveLeft(model));
//                model.movePlayerLeft();
                break;
            case "RIGHT":
                model.registerCmd(new PlayerMoveRight(model));
//                model.movePlayerRight();
                break;
            case "M":
                model.togglePlayerShootingMode();
                break;
            default:
                //nothing
        }
    }
}
