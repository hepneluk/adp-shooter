package cz.cvut.fit.miadp.mvcgame.model.gameObjects.enemy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.strategy.IMoveStrategy;

public class Not extends Enemy {

    public Not(Position position) {
        super(position,(int)MvcGameConfig.images.get(MvcGameConfig.NOT_IMAGE_PATH).getWidth(),
                       (int)MvcGameConfig.images.get(MvcGameConfig.NOT_IMAGE_PATH).getHeight(), 1, 1);
        imagePath = MvcGameConfig.NOT_IMAGE_PATH;
    }

    public Not(Position position, IMoveStrategy moveStrategy) {
        super(position,(int)MvcGameConfig.images.get(MvcGameConfig.NOT_IMAGE_PATH).getWidth(),
                       (int)MvcGameConfig.images.get(MvcGameConfig.NOT_IMAGE_PATH).getHeight(), 1, 1, moveStrategy);
        imagePath = MvcGameConfig.NOT_IMAGE_PATH;
    }

    @Override
    public Object clone(){
        return new Not(getPosition(), moveStrategy);
    }
}
