package cz.cvut.fit.miadp.mvcgame.model;

import cz.cvut.fit.miadp.mvcgame.builder.EnemyPlacer;
import cz.cvut.fit.miadp.mvcgame.command.AbsCommand;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.factory.GameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.factory.IGameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.collision.Collision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.Player;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.collision.MissileCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.enemy.Enemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.gameInfo.ScoreCounter;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.missile.Missile;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;

import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;

public class GameModel implements IGameModel {
    private List<IObserver> myObservers;

    private IGameObjectFactory gameObjectFactory = new GameObjectFactory();
    private EnemyPlacer enemyPlacer = new EnemyPlacer();

    private Player player;
    private ScoreCounter scoreInfo;
    private List<Enemy> enemies;
    private List<Missile> missiles;
    private List<Collision> collisions;

    private Queue<AbsCommand> unexecutedCmds = new LinkedBlockingQueue<>();
    private Deque<AbsCommand> executedCmds = new LinkedBlockingDeque<>();

    private boolean secondChance = true;

    public GameModel() {
        myObservers = new ArrayList<>();

        newGame();
    }

    @Override
    public void registerCmd(AbsCommand command) {
        unexecutedCmds.add(command);
    }

    @Override
    public void goBackInTime() {
        // poll the oldest command
        AbsCommand cmd = executedCmds.pollFirst();
        if (cmd != null)
            cmd.unexecute();

        executedCmds.clear();
        notifyMyObservers();
    }

    private void processCmds() {
        while(!unexecutedCmds.isEmpty()) {
            AbsCommand cmd = unexecutedCmds.poll();
            cmd.doExecute();
            if (executedCmds.size() > MvcGameConfig.NUM_OF_SAVED_CMDS)
                executedCmds.removeFirst();
            executedCmds.addLast(cmd);
        }
    }

    private void newGame() {
        player = gameObjectFactory.createPlayer();
        scoreInfo = gameObjectFactory.createScoreCounter();
        scoreInfo.registerObserver(this);

        enemyPlacer.reset();
        enemies = enemyPlacer.createEnemies();
        missiles = new ArrayList<>();
        collisions = new ArrayList<>();
    }

    @Override
    public void timeTick() {
        processCmds();

        moveMissiles();
        moveEnemies();
        checkForCollisions();
        deleteObsoleteObjects();
        missiles.addAll(player.shoot());
        if (checkForPlayerCollisions())
            playerGotHit();
    }

    @Override
    public void movePlayerUp() {
        player.moveUp();
        notifyMyObservers();
    }

    @Override
    public void movePlayerDown() {
        player.moveDown();
        notifyMyObservers();
    }

    @Override
    public void movePlayerLeft() {
        player.moveLeft();
        notifyMyObservers();
    }

    @Override
    public void movePlayerRight() {
        player.moveRight();
        notifyMyObservers();
    }

    @Override
    public void registerObserver(IObserver observer) {
        if (!myObservers.contains(observer))
            myObservers.add(observer);
    }

    @Override
    public void unregisterObserver(IObserver observer) {
        myObservers.remove(observer);
    }

    @Override
    public void notifyMyObservers() {
        for (IObserver obs : myObservers) {
            obs.update();
        }
    }

    @Override
    public List<GameObject> getGameObjects() {
        List<GameObject> gameObjects = new ArrayList<>();

        gameObjects.addAll(enemies);
        gameObjects.addAll(missiles);
        gameObjects.addAll(collisions);
        gameObjects.add(player);
        gameObjects.add(scoreInfo);

        return gameObjects;
    }

    private void moveMissiles() {
        for (Missile m : missiles) {
            if (!enemies.isEmpty()) {
                // TODO: choose enemy randomly
                m.move(enemies.get(0));
            } else
                m.move((GameObject) null);
        }

        notifyMyObservers();
    }

    private void moveEnemies() {
        for (Enemy e : enemies) {
            e.move(player);
        }

        notifyMyObservers();
    }

    private void deleteObsoleteObjects() {
        for (int i = missiles.size() - 1; i >= 0; i--)
            if (missiles.get(i).isObsolete())
                missiles.remove(i);

        for (int i = enemies.size() - 1; i >= 0; i--)
            if (enemies.get(i).isObsolete())
                removeEnemy(i);

        for (int i = collisions.size() - 1; i >= 0; i--)
            if (collisions.get(i).isObsolete())
                collisions.remove(i);
    }

    @Override
    public void togglePlayerShootingMode() {
        player.toggleShootingMode();
    }

    private void checkForCollisions() {
        for (int i = missiles.size() - 1; i >= 0; i--) {
            for (int j = enemies.size() - 1; j >= 0; j--) {
                if (enemies.get(j).collidesWith(missiles.get(i))) {
                    enemies.get(j).gotHit(missiles.get(i));
                    collisions.add(new MissileCollision(missiles.get(i).getPosition()));
                    missiles.remove(i);
                    break;
                }
            }
        }
    }

    private boolean checkForPlayerCollisions() {
        for (Enemy e : enemies) {
            if (e.collidesWith(player)) {
                return true;
            }
        }
        return false;
    }

    private void playerGotHit() {
        if (secondChance)
            secondChance();
        else {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {}

            newGame();
        }
    }

    private void secondChance() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {}

        secondChance = false;
        goBackInTime();
    }

    private void removeEnemy(int enemyIndex) {
        if (enemies.get(enemyIndex).isDead())
            scoreInfo.addScore(enemies.get(enemyIndex).getValue());

        enemyPlacer.enemyOut(enemies.get(enemyIndex), enemies);
        enemies.remove(enemyIndex);
    }

    @Override
    public void update() {
        enemyPlacer.nextLevel();
    }

    private class Memento {
        private ScoreCounter score;
        private Player player;
        private List<Enemy> enemies = new ArrayList<>();
        private List<Missile> missiles = new ArrayList<>();
        private List<Collision> collisions = new ArrayList<>();
    }

    public Object createMemento() {
        Memento m = new Memento();
        m.score = (ScoreCounter) scoreInfo.getClone();
        m.player = (Player) player.getClone();
        for (Enemy e : enemies)
            m.enemies.add((Enemy)e.clone());
        for (Missile missile : missiles)
            m.missiles.add((Missile)missile.getClone());
        for (Collision c : collisions)
            m.collisions.add((Collision)c.getClone());

        return m;
    }

    public void setMemento(Object memento) {
        Memento m = (Memento) memento;
        scoreInfo = m.score;
        player = m.player;
        enemies = m.enemies;
        missiles = m.missiles;
        collisions = m.collisions;
    }
}
