package cz.cvut.fit.miadp.mvcgame.config;

import javafx.scene.image.Image;

import java.util.HashMap;

public class MvcGameConfig
{
    public static final HashMap<String, Image> images = new HashMap<>() {
        {
            put(PLAYER_IMAGE_PATH, new Image(PLAYER_IMAGE_PATH));
            put(NOT_IMAGE_PATH, new Image(NOT_IMAGE_PATH));
            put(AND_IMAGE_PATH, new Image(AND_IMAGE_PATH));
            put(OR_IMAGE_PATH, new Image(OR_IMAGE_PATH));
        }
    };
    public static final int MAX_X = 1000;
    public static final int MAX_Y = 1000;
    public static final int NUM_OF_SAVED_CMDS = 10;

    // PLAYER
    public static final String PLAYER_IMAGE_PATH = "images/xor.png";
    public static final int PLAYER_SPEED = 8;
    public static final int PLAYER_WIDTH = (int) images.get("images/xor.png").getWidth();
    public static final int PLAYER_HEIGHT = (int) images.get("images/xor.png").getHeight();
    public static final int PLAYER_START_Y = (int) (MAX_Y * .8);
    public static final int PLAYER_START_X = MAX_X / 2 - PLAYER_WIDTH / 2;

    public static final int SCORE_COUNTER_X = MAX_X / 2;
    public static final int SCORE_COUNTER_Y = (int) (MAX_Y * .02);

    public static final int MISSILE_SPEED = 15;
    public static final long MISSILE_MAX_LIFE_SPAN = 2000;

    // collision
    public static final int COLLISION_DURATION = 175;

    // enemies
    public static final String NOT_IMAGE_PATH = "images/not.png";
    public static final String AND_IMAGE_PATH = "images/and.png";
    public static final String OR_IMAGE_PATH = "images/or.png";
    public static final int LEVEL_START = 1;
    public static final int VALUE_OF_ENEMIES_START = 5;
    public static final int ENEMY_SPEED_SLOW = 2;
    public static final int ENEMY_SPEED_NORMAL = 4;
    public static final int ENEMY_SPEED_FAST = 6;
}