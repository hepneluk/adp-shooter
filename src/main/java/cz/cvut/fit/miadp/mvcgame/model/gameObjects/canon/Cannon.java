package cz.cvut.fit.miadp.mvcgame.model.gameObjects.canon;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.missile.Missile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.missile.SmallMissile;
import cz.cvut.fit.miadp.mvcgame.state.AimingShootingMode;
import cz.cvut.fit.miadp.mvcgame.strategy.AimingMissileStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMissileMoveStrategy;
import cz.cvut.fit.miadp.mvcgame.state.DoubleShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.SingleShootingMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Cannon extends AbsCannon {
    private IShootingMode singleShootingMode = new SingleShootingMode();
    private IShootingMode doubleShootingMode = new DoubleShootingMode();
    private IShootingMode aimedShootingMode = new AimingShootingMode();
    private IShootingMode mode;

    public Cannon(Position position, IShootingMode mode) {
        super(position, 10);
        this.mode = mode;
    }

    @Override
    public List<Missile> shoot() {
        if (!isOverheated())
            return mode.shoot(this);
        return Collections.emptyList();
    }

    @Override
    public void toggleShootingMode() {
        mode.toggle(this);
    }

    @Override
    public void setSingleShootingMode() {
        mode = singleShootingMode;
    }

    @Override
    public void setDoubleShootingMode() {
        mode = doubleShootingMode;
    }

    @Override
    public void setAimedShootingMode() {
        mode = aimedShootingMode;
    }

    public ArrayList<Missile> primitiveShoot() {
        ArrayList<Missile> res = new ArrayList<>();
        res.add(new SmallMissile(new Position(position.getX() + MvcGameConfig.PLAYER_WIDTH / 2,
                position.getY()), SimpleMissileMoveStrategy.getInstance()));
        return res;
    }

    public ArrayList<Missile> aimedShoot() {
        ArrayList<Missile> res = new ArrayList<>();
        res.add(new SmallMissile(new Position(position.getX() + MvcGameConfig.PLAYER_WIDTH / 2,
                position.getY()), AimingMissileStrategy.getInstance()));
        return res;
    }

    @Override
    public List<Missile> doubleShoot() {
        ArrayList<Missile> res = primitiveShoot();
        res.addAll(primitiveShoot());
        res.get(0).move(- res.get(0).getWidth(), 0);
        res.get(1).move(res.get(1).getWidth(), 0);
        return res;
    }

    @Override
    protected Object clone() {
        return new Cannon(getPosition(), mode);
    }

    @Override
    public Object getClone() {
        return clone();
    }

    @Override
    public Object getClone(Position position) {
        return new Cannon(position, mode);
    }

    @Override
    public IShootingMode getShootingMode() {
        return mode;
    }
}
