package cz.cvut.fit.miadp.mvcgame.state;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.canon.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.missile.Missile;

import java.util.List;

public interface IShootingMode {
    List<Missile> shoot(AbsCannon canon);
    void toggle(AbsCannon canon);
}
