package cz.cvut.fit.miadp.mvcgame.model;

import cz.cvut.fit.miadp.mvcgame.command.AbsCommand;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.observer.IObservable;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;

import java.util.List;

public interface IGameModel extends IObservable, IObserver {
    void registerCmd(AbsCommand command);

    void goBackInTime();

    void timeTick();

    void movePlayerUp();

    void movePlayerDown();

    void movePlayerLeft();

    void movePlayerRight();

    @Override
    void registerObserver(IObserver observer);

    @Override
    void unregisterObserver(IObserver observer);

    @Override
    void notifyMyObservers();

    List<GameObject> getGameObjects();

    void togglePlayerShootingMode();

    @Override
    void update();

    Object createMemento();

    void setMemento(Object object);
}
