package cz.cvut.fit.miadp.mvcgame.model.gameObjects.missile;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.strategy.IMoveStrategy;

public class SmallMissile extends Missile {
    private final IMoveStrategy moveStrategy;

    public SmallMissile(Position position, IMoveStrategy strategy) {
        super(position, 3, 6, 1);
        this.moveStrategy = strategy;
    }

    @Override
    public void move(GameObject gameObject) {
        this.moveStrategy.updatePosition(position, gameObject);
    }

    @Override
    protected Object clone() {
        return new SmallMissile(getPosition(), moveStrategy);
    }
}
