package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.collision.Collision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.Player;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.enemy.Enemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.gameInfo.GameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.gameInfo.ScoreCounter;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.missile.Missile;

public interface IVisitor {

    void visitPlayer(Player player);
    void visitEnemy(Enemy enemy);
    void visitMissile(Missile missile);
    void visitCollision(Collision collision);
    void visitGameInfo(GameInfo gameInfo);
    void visitScoreInfo(ScoreCounter scoreCounter);

    void setGraphics(IGameGraphics gr);
}
