package cz.cvut.fit.miadp.mvcgame;

import java.util.List;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.controller.GameController;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
// in future, use Bridge to remove this dependency
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameModelProxy;
import cz.cvut.fit.miadp.mvcgame.view.GameView;
import javafx.scene.canvas.GraphicsContext;

public class MvcGame
{
    private IGameModel model;
    private GameController controller;
    private GameView view;

    public void init()
    {
        this.model = new GameModelProxy();
        this.view = new GameView(model);
        this.controller = view.makeController();
    }

    public void processPressedKeys(List<String> pressedKeysCodes)
    {
        boolean containsM = false;
        for(String code : pressedKeysCodes)
        {
            controller.handleKeyCode(code);
            if (code.equals("M"))
                containsM = true;
        }

        if (containsM) {
            pressedKeysCodes.remove("M");
        }
    }

    public void update(long time)
    {
        model.timeTick();
    }

    public void render(IGameGraphics gr)
    {
        view.setGraphics(gr);
        view.render();
    }

    public String getWindowTitle()
    {
        return "War of shapes";
    }

    public int getWindowWidth()
    {
        return MvcGameConfig.MAX_X;
    }

    public int getWindowHeight()
    {
        return  MvcGameConfig.MAX_Y;
    }
}