package cz.cvut.fit.miadp.mvcgame.model.gameObjects.enemy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.strategy.IMoveStrategy;

public class Or extends Enemy {

    public Or(Position position) {
        super(position, (int)MvcGameConfig.images.get(MvcGameConfig.OR_IMAGE_PATH).getWidth(),
                        (int)MvcGameConfig.images.get(MvcGameConfig.OR_IMAGE_PATH).getHeight(),4, 4);
        imagePath = MvcGameConfig.OR_IMAGE_PATH;
    }

    public Or(Position position, IMoveStrategy moveStrategy) {
        super(position, (int)MvcGameConfig.images.get(MvcGameConfig.OR_IMAGE_PATH).getWidth(),
                        (int)MvcGameConfig.images.get(MvcGameConfig.OR_IMAGE_PATH).getHeight(),4, 4, moveStrategy);
        imagePath = MvcGameConfig.OR_IMAGE_PATH;
    }

    @Override
    public Object clone(){
        return new Or(getPosition(), moveStrategy);
    }
}
