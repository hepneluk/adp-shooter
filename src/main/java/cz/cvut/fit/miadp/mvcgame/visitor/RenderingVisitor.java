package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.collision.Collision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.Player;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.enemy.Enemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.gameInfo.GameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.gameInfo.ScoreCounter;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.missile.Missile;

public class RenderingVisitor implements IVisitor {
    private String playerImagePath;
    private IGameGraphics gr;

    public RenderingVisitor() {
        this.playerImagePath = MvcGameConfig.PLAYER_IMAGE_PATH;
    }

    @Override
    public void setGraphics(IGameGraphics gr) {
        this.gr = gr;
    }

    @Override
    public void visitPlayer(Player player) {
        drawPlayer(player);
    }

    @Override
    public void visitEnemy(Enemy enemy) {
        drawEnemy(enemy);
    }

    @Override
    public void visitMissile(Missile missile) {
        drawMissile(missile);
    }

    @Override
    public void visitCollision(Collision collision) {
        drawCollision(collision);
    }

    @Override
    public void visitGameInfo(GameInfo gameInfo) {

    }

    @Override
    public void visitScoreInfo(ScoreCounter scoreCounter) {
        drawScore(scoreCounter);
    }

    private void drawPlayer(Player player) {
        gr.drawImage(playerImagePath, player.getPosition());
    }

    private void drawScore(ScoreCounter gameInfo) {
        gr.setStroke("BLACK");
        gr.strokeText(String.valueOf(gameInfo.getScore()), gameInfo.getPosition());
    }

    private void drawMissile(Missile missile) {
        gr.fillRect(missile.getPosition(), missile.getWidth(), missile.getHeight());
    }

    private void drawEnemy(Enemy enemy) {
        gr.drawImage(enemy.getImagePath(), enemy.getPosition());
    }

    private void drawCollision(Collision collision) {
        int len = 5;
        gr.setStroke("RED");
        gr.strokeLine(collision.getTopLeft().move(len, len), collision.getTopLeft());
        gr.strokeLine(collision.getTopRight().move(- len, len), collision.getTopRight());
        gr.strokeLine(collision.getBottomRight().move(-len, - len), collision.getBottomRight());
        gr.strokeLine(collision.getBottomLeft().move(len, - len), collision.getBottomLeft());
    }
}
