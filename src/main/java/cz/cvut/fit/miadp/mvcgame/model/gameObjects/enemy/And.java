package cz.cvut.fit.miadp.mvcgame.model.gameObjects.enemy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.strategy.IMoveStrategy;

public class And extends Enemy {

    public And(Position position) {
        super(position, (int)MvcGameConfig.images.get(MvcGameConfig.AND_IMAGE_PATH).getWidth(),
                        (int)MvcGameConfig.images.get(MvcGameConfig.AND_IMAGE_PATH).getHeight(), 2, 2);
        imagePath = MvcGameConfig.AND_IMAGE_PATH;
    }

    public And(Position position, IMoveStrategy moveStrategy) {
        super(position, (int)MvcGameConfig.images.get(MvcGameConfig.AND_IMAGE_PATH).getWidth(),
                        (int)MvcGameConfig.images.get(MvcGameConfig.AND_IMAGE_PATH).getHeight(), 2, 2, moveStrategy);
        imagePath = MvcGameConfig.AND_IMAGE_PATH;
    }

    @Override
    public Object clone(){
        return new And(getPosition(), moveStrategy);
    }
}
