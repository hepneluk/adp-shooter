package cz.cvut.fit.miadp.mvcgame.model.gameObjects.canon;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.missile.Missile;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

import java.util.List;

public abstract class AbsCannon extends GameObject {
    protected int maxFireRate;
    protected int fireRate;

    public AbsCannon(Position position, int fireRate) {
        super(position);
        this.fireRate = fireRate;
        this.maxFireRate = fireRate;
    }

    public abstract void toggleShootingMode();
    public abstract List<Missile> shoot();
    public abstract List<Missile> primitiveShoot();
    public abstract List<Missile> doubleShoot();
    public abstract List<Missile> aimedShoot();
    public abstract void setSingleShootingMode();
    public abstract void setDoubleShootingMode();
    public abstract void setAimedShootingMode();
    public abstract IShootingMode getShootingMode();

    public boolean isOverheated() {
        if (fireRate == 0) {
            fireRate = maxFireRate;
            return false;
        }
        fireRate--;
        return true;
    }

    @Override
    public void accept(IVisitor visitor) { /* cannons don't render */}

    @Override
    public abstract Object getClone();
    public abstract Object getClone(Position position);

    @Override
    protected abstract Object clone();
}
