package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public abstract class AbsCommand {
    protected IGameModel receiver;
    protected Object memento;

    public AbsCommand(IGameModel rec) {
        receiver = rec;
    }

    public void doExecute() {
        memento = receiver.createMemento();
        execute();
    }

    public void unexecute() {
        if (memento != null) {
            receiver.setMemento(memento);
        }
    }

    public abstract void execute();
}
