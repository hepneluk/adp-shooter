package cz.cvut.fit.miadp.mvcgame.factory;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.collision.Collision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.Player;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.canon.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.canon.Cannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.enemy.Enemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.gameInfo.GameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.gameInfo.ScoreCounter;
import cz.cvut.fit.miadp.mvcgame.state.SingleShootingMode;

public class GameObjectFactory implements IGameObjectFactory {
    @Override
    public Enemy createEnemy() {
        return null;
    }

    @Override
    public Collision createCollision() {
        return null;
    }

    @Override
    public GameInfo createGameInfo() {
        return new ScoreCounter();
    }

    @Override
    public Player createPlayer() {
        return new Player();
    }

    @Override
    public AbsCannon createCannon(Position position) {
        return new Cannon(position, new SingleShootingMode());
    }

    @Override
    public ScoreCounter createScoreCounter() {
        return new ScoreCounter();
    }
}
