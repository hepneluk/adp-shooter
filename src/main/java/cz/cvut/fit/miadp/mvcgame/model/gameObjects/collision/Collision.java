package cz.cvut.fit.miadp.mvcgame.model.gameObjects.collision;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.LifetimeLimitedGameObject;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public abstract class Collision extends LifetimeLimitedGameObject {

    public Collision(Position position, int width, int height) {
        super(position, width, height);
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visitCollision(this);
    }

    @Override
    public boolean isObsolete() {
        return getAge() > MvcGameConfig.COLLISION_DURATION || isOutOfView();
    }

    @Override
    protected abstract Object clone();

    @Override
    public Object getClone() {
        return clone();
    }
}
