package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;

public class FastEnemyMoveStrategy implements IMoveStrategy {
    private static FastEnemyMoveStrategy instance = null;

    private FastEnemyMoveStrategy() {
    }

    @Override
    public void updatePosition(Position position, GameObject gameObject) {
        position.move(0, MvcGameConfig.ENEMY_SPEED_FAST);
    }

    @Override
    public int value() {
        return 3;
    }

    public static FastEnemyMoveStrategy getInstance() {
        if (instance == null)
            instance = new FastEnemyMoveStrategy();
        return instance;
    }
}
