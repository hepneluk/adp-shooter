package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;

public class NormalSpeedEnemyStrategy implements IMoveStrategy {
    private static NormalSpeedEnemyStrategy instance = null;

    private NormalSpeedEnemyStrategy() {
    }

    @Override
    public void updatePosition(Position position, GameObject gameObject) {
        position.move(0, MvcGameConfig.ENEMY_SPEED_NORMAL);
    }

    @Override
    public int value() {
        return 2;
    }

    public static IMoveStrategy getInstance() {
        if (instance == null)
            instance = new NormalSpeedEnemyStrategy();
        return instance;
    }
}
