package cz.cvut.fit.miadp.mvcgame.model.gameObjects.missile;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.LifetimeLimitedGameObject;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public abstract class Missile extends LifetimeLimitedGameObject {
    protected int damage;

    public Missile(Position position, int width, int height, int damage) {
        super(position, width, height);
        this.damage = damage;
    }

    public int getDamage() {
        return damage;
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visitMissile(this);
    }

    @Override
    public boolean isObsolete() {
        return getAge() > MvcGameConfig.MISSILE_MAX_LIFE_SPAN || isOutOfView();
    }

    public abstract void move(GameObject gameObject);

    @Override
    protected abstract Object clone();

    @Override
    public Object getClone() {
        return clone();
    }
}
