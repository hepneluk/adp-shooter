package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.factory.GameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.factory.IGameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.canon.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.canon.Cannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.missile.Missile;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

import java.util.List;

public class Player extends GameObject {
    private AbsCannon cannon;

    public Player() {
        super(new Position(MvcGameConfig.PLAYER_START_X, MvcGameConfig.PLAYER_START_Y),
                MvcGameConfig.PLAYER_WIDTH, MvcGameConfig.PLAYER_HEIGHT);
        IGameObjectFactory factory = new GameObjectFactory();
        // share positions between player and his cannon
        cannon = factory.createCannon(position);
    }

    public Player(Position position, AbsCannon cannon) {
        super(position);
        this.cannon = cannon;
    }

    public void moveUp() {
        this.move(0, -MvcGameConfig.PLAYER_SPEED);
        if (isOutOfView()) {
            this.position.setY(0);
        }
    }

    public void moveDown() {
        this.move(0, MvcGameConfig.PLAYER_SPEED);
        if (isOutOfView()) {
            this.position.setY(MvcGameConfig.MAX_Y - height);
        }
    }

    public void moveLeft() {
        this.move(-MvcGameConfig.PLAYER_SPEED, 0);
        if (isOutOfView()) {
            this.position.setX(0);
        }
    }

    public void moveRight() {
        this.move(MvcGameConfig.PLAYER_SPEED, 0);
        if (isOutOfView()) {
            this.position.setX(MvcGameConfig.MAX_X - width);
        }
    }

    public List<Missile> shoot() {
        return cannon.shoot();
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visitPlayer(this);
    }

    @Override
    public boolean isOutOfView() {
        return position.getX() + width > MvcGameConfig.MAX_X || position.getX() < 0 ||
                position.getY() + height > MvcGameConfig.MAX_Y || position.getY() < 0;
    }

    public void toggleShootingMode() {
        cannon.toggleShootingMode();
    }

    @Override
    protected Object clone() {
        Position newPos = getPosition();
        return new Player(newPos, (Cannon) cannon.getClone(newPos));
    }

    @Override
    public Object getClone() {
        return this.clone();
    }
}
