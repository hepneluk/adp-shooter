package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.model.Position;

public interface IGameGraphicsImplementor {
    void drawImage(String path, Position pos);
    void drawText(String text, Position pos);
    void strokeText(String text, Position pos);
    void drawLine(Position start, Position end);
    void strokeLine(Position start, Position end);
    void clearRect(Position topLeft, Position bottomRight);
    void fillRect(Position leftTop, Position bottomRight);
    void fillRect(Position leftTop, int width, int height);
    void setStroke(String color);
}
