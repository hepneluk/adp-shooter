package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.HashMap;

public class JavaFxGraphics implements IGameGraphicsImplementor {
    protected GraphicsContext gc;
    private HashMap<String, Color> colors = new HashMap<>() {
        {
            put("RED", Color.RED);
            put("BLACK", Color.BLACK);
        }
    };

    public JavaFxGraphics(GraphicsContext gc) {
        this.gc = gc;
    }

    @Override
    public void drawImage(String path, Position pos) {
        gc.drawImage(MvcGameConfig.images.get(path), pos.getX(), pos.getY());
    }

    @Override
    public void drawText(String text, Position pos) {
        gc.fillText(text, pos.getX(), pos.getY());
    }

    @Override
    public void drawLine(Position start, Position end) {
        gc.strokeLine(start.getX(), start.getY(), end.getX(), end.getY());
    }

    @Override
    public void clearRect(Position topLeft, Position bottomRight) {
        gc.clearRect(topLeft.getX(), topLeft.getY(), bottomRight.getX() - topLeft.getX(), bottomRight.getY() - topLeft.getY());
    }

    @Override
    public void strokeText(String text, Position pos) {
        gc.strokeText(text, pos.getX(), pos.getY());
    }

    @Override
    public void strokeLine(Position start, Position end) {
        gc.strokeLine(start.getX(), start.getY(), end.getX(), end.getY());
    }

    @Override
    public void fillRect(Position leftTop, Position bottomRight) {
        gc.fillRect(leftTop.getX(), leftTop.getY(), bottomRight.getX() - leftTop.getX(), bottomRight.getY() - leftTop.getY());
    }

    @Override
    public void fillRect(Position leftTop, int width, int height) {
        gc.fillRect(leftTop.getX(), leftTop.getY(), width, height);
    }

    @Override
    public void setStroke(String color) {
        gc.setStroke(colors.get(color));
    }
}
