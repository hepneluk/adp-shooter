package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.bridge.GameGraphics;
import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphicsImplementor;
import cz.cvut.fit.miadp.mvcgame.bridge.JavaFxGraphics;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;

import cz.cvut.fit.miadp.mvcgame.MvcGame;
import javafx.util.Duration;

public class MvcGameJavaFxLauncher extends Application {

    private static final MvcGame theMvcGame = new MvcGame();

    @Override
    public void init() {
        theMvcGame.init();
    }

    @Override
    public void start(Stage stage) {
        String winTitle = theMvcGame.getWindowTitle();
        int winWidth = theMvcGame.getWindowWidth();
        int winHeight = theMvcGame.getWindowHeight();

        stage.setTitle( winTitle );

        Group root = new Group();
        Scene theScene = new Scene( root );
        stage.setScene( theScene );
            
        Canvas canvas = new Canvas( winWidth, winHeight );
        root.getChildren().add( canvas );
            
        IGameGraphicsImplementor gr_impl = new JavaFxGraphics(canvas.getGraphicsContext2D());
        IGameGraphics gr = new GameGraphics(gr_impl);


        ArrayList<String> pressedKeysCodes = new ArrayList<String>();
 
        theScene.setOnKeyPressed(
            new EventHandler<KeyEvent>()
            {
                public void handle(KeyEvent e)
                {
                    String code = e.getCode().toString();
 
                    // only add once... prevent duplicates
                    if ( !pressedKeysCodes.contains(code) && !code.equals("M"))
                        pressedKeysCodes.add( code );
                }
            }
        );
 
        theScene.setOnKeyReleased(
            new EventHandler<KeyEvent>()
            {
                public void handle(KeyEvent e)
                {
                    String code = e.getCode().toString();
                    if (code.equals("M")) {
                        pressedKeysCodes.add( code );
                    }
                    else {
                        pressedKeysCodes.remove(code);
                    }
                }
            }
        );

        // the game-loop
        new AnimationTimer()
        {
            public void handle(long currentNanoTime)
            {
                theMvcGame.processPressedKeys(pressedKeysCodes);
                theMvcGame.update(currentNanoTime);
                theMvcGame.render(gr);
            }
        }.start();
            
        stage.show();
    }

    public static void main(String[] args) {
        Media sound = new Media(new File("C:\\School\\MI-ADP\\miadp-mvcgame-b191-java\\src\\main\\resources\\sounds\\music.mp3").toURI().toString());
        MediaPlayer player = new MediaPlayer(sound);
        player.setOnEndOfMedia(new Runnable() {
            @Override
            public void run() {
                player.seek(Duration.ZERO);
                player.play();
            }
        });
        player.play();
        launch();
    }

}